const dev = {
    s3: {
        REGION: "us-east-1",
        BUCKET: "notes-app-2-api-dev-attachmentsbucket-9hv8cakdhij8"
    },
    apiGateway: {
        REGION: "us-east-1",
        URL: "https://3t7gfifpz9.execute-api.us-east-1.amazonaws.com/dev"
    },
    cognito: {
        REGION: "us-east-1",
        USER_POOL_ID: "us-east-1_jdgrIlQff",
        APP_CLIENT_ID: "7u18dg2lj5gsi6fc5l6qb6j6vr",
        IDENTITY_POOL_ID: "us-east-1:d47371d6-b627-4ff6-ac09-493d3bc2ec83"
    },
    STRIPE_KEY: "pk_test_6mTNIN1EguthuutpjFfD1VPh"
};

const prod = {
    s3: {
        REGION: "us-east-1",
        BUCKET: "notes-app-2-api-prod-attachmentsbucket-nq5yu4i5j4ym"
    },
    apiGateway: {
        REGION: "us-east-1",
        URL: "https://atpf25ife4.execute-api.us-east-1.amazonaws.com/prod"
    },
    cognito: {
        REGION: "us-east-1",
        USER_POOL_ID: "us-east-1_tWFiOoxA3",
        APP_CLIENT_ID: "7j1mmff5f84gfq2mafl3bksn4c",
        IDENTITY_POOL_ID: "us-east-1:3bc3fc79-54cf-42dc-bb1d-20d15b234808"
    },
    STRIPE_KEY:"pk_test_6mTNIN1EguthuutpjFfD1VPh"
};

// Default to dev if not set
const config = process.env.REACT_APP_STAGE === 'prod'
    ? prod
    : dev;

export default {
    // Add common config values here
    MAX_ATTACHMENT_SIZE: 5000000,
    ...config
};