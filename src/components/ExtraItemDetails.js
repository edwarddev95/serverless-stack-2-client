import React from "react";
import {
    Image,
    Form,
    List,
    Grid,
    Header,
    Rating,
    Button,
    Checkbox,
    Icon,
    Label,
    Divider,
    Message
} from "semantic-ui-react";

import Select from "react-select";
import ReactDropzone from "react-dropzone"
let conditionOptions = [
    {"value": "vgc", "label": "Very Good Condition"},
    {"value": "gc", "label": "Good Condition" },
    {"value": "new", "label": "Brand New"}

]


export default class ExtraItemDetails extends React.Component {
    constructor() {
        super();

    }

    render()

    {
        console.log(this.props.selected.files[0]);

        return (
            <React.Fragment>
            <Grid.Row >
                <Grid.Column floated="left" width={4}>
                    <Message info>
                        <Message.Header>Step 2</Message.Header>
                        <p>Rate the condition of your item and let us know if you still have the original receipt or packaging such as shoe boxes or dustbags.</p>
                    </Message>
                </Grid.Column>
                <Grid.Column floated="left" width={5}>
                    <Select
                        className="basic-single"
                        classNamePrefix="select"
                        placeholder="Select Condition"
                        isSearchable={false}
                        value={this.props.condition}
                        name="category"
                        onChange={this.props.handleSelectChange.bind(this, "condition")}
                        options={conditionOptions}
                    />
                    <Form.Group inline>
                        <Form.Field control={Checkbox} label={<label>Proof of Purchase</label>}/>
                        <Form.Field
                            control={Checkbox}
                            label={<label>Original Tags/Packaging</label>}
                        />
                    </Form.Group>
                </Grid.Column>
            </Grid.Row>

                <Grid.Row columns={2} >
                    <Grid.Column floated="left" width={4}>
                        <Message info>
                            <Message.Header>Step 3</Message.Header>
                            <p>Upload some photos by clicking on the camera icon</p>
                        </Message>
                    </Grid.Column>
                    <Grid.Column floated="left"  width={12}>
                    <ReactDropzone style={{}} onDrop={this.props.onPreviewDrop.bind(this)} accept="image/*">
                        <Image inline style={{margin:"2%"}} centered bordered size="small" src={this.props.selected.files[0] !== void 0 ? this.props.selected.files[0].preview : "./image_upload_placeholder.jpg"}/>
                        <Image inline style={{margin:"2%"}} centered bordered size="small" src={this.props.selected.files[1] !== void 0 ? this.props.selected.files[1].preview : "./image_upload_placeholder.jpg"}/>
                        <Image inline style={{margin:"2%"}} centered bordered size="small" src={this.props.selected.files[2] !== void 0 ? this.props.selected.files[2].preview : "./image_upload_placeholder.jpg"}/>
                        <Image inline style={{margin:"2%"}} centered bordered size="small" src={this.props.selected.files[3] !== void 0 ? this.props.selected.files[3].preview : "./image_upload_placeholder.jpg"}/>
                    </ReactDropzone>
                    </Grid.Column>
                </Grid.Row>
            </React.Fragment>
        );
    }
}
