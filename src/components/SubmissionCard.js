import React from "react";
import {
  Image,
  Form,
  List,
  Grid,
  Header,
  Rating,
    Button,
  Checkbox,
    Icon,
    Label
} from "semantic-ui-react";

import Select from "react-select";
import ReactDropzone from "react-dropzone"
let conditionOptions = [
    {"value": "vgc", "label": "Very Good Condition"},
    {"value": "gc", "label": "Good Condition" },
    {"value": "new", "label": "Brand New"}

]


export default class SubmissionCard extends React.Component {
    constructor() {
        super();
        this.state = {
            condition: "",
            receipt: "",
            tags: "",
            files: []
        };
    }
    handleChange = (e, { value }) => this.setState({ value });

    onPreviewDrop = (files) => {
        this.setState({
            files: this.state.files.concat(files),
        });
    };


    render()

    {
        console.log(this.state);
        return (

            <Grid.Row columns = {2}>
                <Grid.Column width={4}>
                    <Select
                        className="basic-single"
                        classNamePrefix="select"
                        placeholder="Select Condition"
                        value={this.state.condition.value}
                        name="category"
                        onChange={this.handleChange.bind(this, "condition")}
                        options={conditionOptions}
                    />
                    <Form.Group inline>
                        <Form.Field control={Checkbox} label={<label>Proof of Purchase</label>}/>
                    <Form.Field
                        control={Checkbox}
                        label={<label>Original Tags/Packaging</label>}
                    />
                    </Form.Group>
                </Grid.Column>
                <Grid.Column width={12} >
                    <ReactDropzone style={{}} onDrop={this.onPreviewDrop} accept="image/*">
                    <Image inline style={{margin:"2%"}} centered size="small" src={this.state.files[0] !== void 0 ? this.state.files[0].preview : "./image_upload_placeholder.jpg"}/>
                    <Image inline style={{margin:"2%"}} centered size="small" src={this.state.files[1] !== void 0 ? this.state.files[1].preview : "./image_upload_placeholder.jpg"}/>
                    <Image inline style={{margin:"2%"}} centered size="small" src={this.state.files[2] !== void 0 ? this.state.files[2].preview : "./image_upload_placeholder.jpg"}/>
                    <Image inline style={{margin:"2%"}} centered size="small" src={this.state.files[3] !== void 0 ? this.state.files[3].preview : "./image_upload_placeholder.jpg"}/>
                    </ReactDropzone>
                </Grid.Column>
            </Grid.Row>
        );
    }
}
