import React from "react";
import { Grid } from "semantic-ui-react";
import SubmissionCard from "./SubmissionCard";
import SubmissionItemDisplay from "./SubmissionItemDisplay";

const SubmissionListDisplay = props => (
        <Grid centered stackable divided="vertically">

            {props.items.map((item, index) => (<SubmissionCard item={item} key={index} id={index}/>))}

        </Grid>
);

export default SubmissionListDisplay;
