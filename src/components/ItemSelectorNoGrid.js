import {Button, Container, Grid, Message} from "semantic-ui-react";
import React, { Component } from "react";
import Select from "react-select";

let categoryOptions = [
  { value: "men-fashion-accessories", label: "Men's Fashion & Accessories" },
  { value: "women-fashion-accessories", label: "Women's Fashion & Accessories" }
];
let designerOptions = [
  { value: "saint_laurent_paris", label: "Saint Laurent Paris" },
  { value: "gucci", label: "Gucci" },
  { value: "thom_browne", label: "Thom Browne" },
  { value: "dior", label: "Dior" },
  { value: "fendi", label: "Fendi" },
  { value: "haider_ackermann", label: "Haider Ackermann" }
];
let typeOptions = [
  {
    category: "men-fashion-accessories",
    options: [
      { value: "outerwear", label: "Outerwear" },
      {
        value: "sweats-knits",
        label: "Knitwear & Sweats"
      },
      { value: "shirts-t-shirts", label: "Shirts & T-Shirts" },
      { value: "trousers", label: "Trousers" },
      { value: "footwear", label: "Footwear" }
    ]
  },
  {
    category: "women-fashion-accessories",
    options: [
      { value: "dress", label: "Dresses" },
      { value: "outerwear", label: "Outerwear" },
      {
        value: "sweats-knits",
        label: "Knitwear & Sweats"
      },
      { value: "shirts-t-shirts", label: "Shirts & T-Shirts" },
      { value: "trousers", label: "Trousers" },
      { value: "footwear", label: "Footwear" }
    ]
  }
];
export default class ItemSelector extends Component {
  constructor() {
    super();
  }
    //
  render() {
    return (
        <Grid.Row >
          <Grid.Column floated="left" width={4}>
              <Message info >
                  <Message.Header>Step 1</Message.Header>
                  <p>Fill in the Category, Brand and Type of item you wish to sell</p>
              </Message>
          </Grid.Column>
          <Grid.Column floated="left" width={5}>
              <Select
                  className="basic-single"
                  classNamePrefix="select"
                  placeholder="Select Category"
                  value={this.props.selected.category}
                  isSearchable={false}
                  name="category"
                  onChange={this.props.handleChange.bind(this, "category")}
                  options={categoryOptions}
              />
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder="Select Designer Brand"
              isSearchable
              value={this.props.selected.designer}
              name="designer"
              isDisabled={this.props.selected.category === ""}
              onChange={this.props.handleChange.bind(this, "designer")}
              options={designerOptions}

            />
              <Select
                  className="basic-single"
                  classNamePrefix="select"
                  placeholder="Select Item Type"
                  isSearchable={false}
                  value={this.props.selected.type}
                  name="type"
                  isDisabled={this.props.selected.designer === ""}
                  onChange={this.props.handleChange.bind(this, "type")}
                  options={typeOptions.filter(obj => {
                      return obj.category === this.props.selected.category.value;
                  })}
              />
          </Grid.Column>
        </Grid.Row>
    );
  }
}
