import React from "react";
import { Header, Grid, Divider } from "semantic-ui-react";
import SubmissionItemDisplay from "./SubmissionItemDisplay";

const SubmissionListDisplay = props => (
  <div style={{ paddingTop: "10%" }}>
    <Header as="h3" dividing textAlign="center">
      Submission List: {props.items.length} items
    </Header>
    <Grid divided="vertically" stackable>
      {props.items.map((item, index) => (
        <SubmissionItemDisplay
          item={item}
          key={index}
          id={index}
          deleteItem={props.deleteItem}
        />
      ))}
    </Grid>
      <Divider />
  </div>
);

export default SubmissionListDisplay;
