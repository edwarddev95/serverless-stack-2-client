import React from "react";
import "./ImageUpload.css";

export class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: ''
        };
        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }

    _handleSubmit(e) {
        e.preventDefault();
        // TODO: do something with -> this.state.file
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    render() {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} className="img-thumbnail" style={{width:'200px', height:'200px'}}/>);
        }

        return (
            <div >
                <div className="row">
                    <label className="btn btn-default btn-file">
                        Choose Image <input type="file" onChange={this._handleImageChange} style={{display:'none'}}/>
                    </label>
                </div>
                <div className="row">
                {$imagePreview}
                </div>
            </div>
        )
    }
}
