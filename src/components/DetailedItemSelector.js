import React, { Component } from "react";
import ItemSelector from "./ItemSelectorNoGrid";
import {Button, Grid, Message} from "semantic-ui-react";

import ExtraItemDetails from "../components/ExtraItemDetails";

export default class DetailedItemSelector extends React.Component {
  state = {
    type: "",
    designer: "",
    category: "",
    condition: "",
    receipt: "",
    tags: "",
    files: [],
      selectionsValid : false
  };

  handleRadioChange = (e, { value }) => this.setState({ value });

  handleSelectChange = (name, inputValue: any) => {
    if (inputValue == null) {
      this.setState({ [name]: "" });
    } else {
      this.setState({ [name]: inputValue });
    }
    if (name === "category") {
        this.setState({ designer: "" });
        this.setState({ type: "" });
    }
  };

    onPreviewDrop = (files) => {
        console.log(files);
        this.setState({
            files: this.state.files.concat(files),
        });
    };

    validateSelections = () => {
        return true;
    };

    addItem = () =>
    {
        let selectedItem = {    type: this.state.type.label,
            designer: this.state.designer.label,
            category: this.state.category.label,
            condition: this.state.condition.label,
            receipt: this.state.receipt.label,
            tags: this.state.tags.label,
            files: this.state.files,};
        console.log(selectedItem);
        this.props.onClick(selectedItem)

        let emptyState = {
            type: "",
            designer: "",
            category: "",
            condition: "",
            receipt: "",
            tags: "",
            files: [],
            selectionsValid : false
        };
        this.setState({})

        this.setState({ type: "" });
        this.setState({ designer: "" });
        this.setState({ category: "" });
        this.setState({ condition: "" });
        this.setState({ receipt: "" });
        this.setState({ tags: "" });
        this.setState({ files: [] });
        this.setState({ selectionsValid : false });



    };

  render() {
    return (
      <React.Fragment>
        <Grid divided="vertically" stackable style={{ paddingTop: "1%" }}>
          <ItemSelector
            handleChange={this.handleSelectChange.bind(this)}
            selected={this.state}
          />
          {this.state.type !== "" && (
              <React.Fragment>
            <ExtraItemDetails
              selected={this.state}
              handleSelectChange={this.handleSelectChange}
              handleRadioChange={this.handleRadioChange}
              onPreviewDrop = {this.onPreviewDrop}
            />
              <Grid.Row centered>
              <Grid.Column computer={4} largeScreen={4} mobile={12}>
              <Button
              fluid
              secondary
              style={{ whiteSpace:'nowrap'}}
              disabled={this.state.condition === ""}
              type="submit"
              onClick = {this.addItem}
              >
              + Add Item
              </Button>
              </Grid.Column>
              </Grid.Row>
              </React.Fragment>
          )}

        </Grid>


      </React.Fragment>
    );
  }
}

