import React from "react";
import {Icon, Grid, Button, Transition} from "semantic-ui-react";
import SubmissionCard from "./SubmissionCard"

export default class SubmissionItemDisplay extends React.Component {

    constructor() {
        super();
        this.state = {
            detailsExpanded: false
        };
    }

    toggleDropdown = () =>
    {
        this.setState({detailsExpanded: !this.state.detailsExpanded})
    };


    render() {

        console.log(this.state.detailsExpanded);
        return (
            <React.Fragment>
            <Grid.Row centered columns={4}>
                <Grid.Column width={1} verticalAlign='middle'>
                    <Icon name="checkmark" size="big" />
                </Grid.Column>
                <Grid.Column verticalAlign='middle' width={8}>
                    {this.props.item.category} / <b>{this.props.item.designer} / {this.props.item.type}</b>
                </Grid.Column>
                <Grid.Column width={3}>
                    <Button
                        fluid
                        icon labelPosition='right'
                        style={{whiteSpace: 'nowrap'}}
                        onClick={this.toggleDropdown}
                        type="submit"
                    >
                        Add Details
                        <Icon name={this.state.detailsExpanded ? 'down arrow':'right arrow'}/>
                    </Button>
                </Grid.Column>
                <Grid.Column width={1} verticalAlign='middle'>
                    <Icon size="large" link name="close" onClick={() => this.props.deleteItem(this.props.id)}/>
                </Grid.Column>
            </Grid.Row>

                {this.state.detailsExpanded && <SubmissionCard />}

            </React.Fragment>


        );
    }
}
