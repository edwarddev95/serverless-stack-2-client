import { Button, Grid } from "semantic-ui-react";
import React, { Component } from "react";
import Select from "react-select";

let categoryOptions = [
  { value: "men-fashion-accessories", label: "Men's Fashion & Accessories" },
  { value: "women-fashion-accessories", label: "Women's Fashion & Accessories" }
];
let designerOptions = [
  { value: "saint_laurent_paris", label: "Saint Laurent Paris" },
  { value: "gucci", label: "Gucci" },
  { value: "thom_browne", label: "Thom Browne" },
  { value: "dior", label: "Dior" },
  { value: "fendi", label: "Fendi" },
  { value: "haider_ackermann", label: "Haider Ackermann" }
];
let typeOptions = [
  {
    category: "men-fashion-accessories",
    options: [
      { value: "outerwear", label: "Outerwear" },
      {
        value: "sweats-knits",
        label: "Knitwear & Sweats"
      },
      { value: "shirts-t-shirts", label: "Shirts & T-Shirts" },
      { value: "trousers", label: "Trousers" },
      { value: "footwear", label: "Footwear" }
    ]
  },
  {
    category: "women-fashion-accessories",
    options: [
      { value: "dress", label: "Dresses" },
      { value: "outerwear", label: "Outerwear" },
      {
        value: "sweats-knits",
        label: "Knitwear & Sweats"
      },
      { value: "shirts-t-shirts", label: "Shirts & T-Shirts" },
      { value: "trousers", label: "Trousers" },
      { value: "footwear", label: "Footwear" }
    ]
  }
];
export default class ItemSelector extends Component {
  constructor() {
    super();
    this.state = {
      category: "",
      designer: "",
      type: ""
    };
  }

  itemAdded = e => {
    e.preventDefault();
    let selectedItem = {
      category: this.state.category.label,
      designer: this.state.designer.label,
      type: this.state.type.label
    };
    this.props.handleSubmit(selectedItem);
  };

  handleChange = (name, inputValue: any) => {
    if (inputValue == null) {
      this.setState({ [name]: "" });
    } else {
      this.setState({ [name]: inputValue });
    }

    if (name === "category") {
      this.setState({ designer: "" });
      this.setState({ type: "" });
    }
  };
    //
  render() {
    console.log(this.state);
    return (
      <Grid stackable style={{paddingTop:'5%'}}>
        <Grid.Row centered>
          <Grid.Column width={4}>
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder="Select Category"
              isSearchable
              value={this.state.category}
              name="category"
              onChange={this.handleChange.bind(this, "category")}
              options={categoryOptions}
            />
          </Grid.Column>
          <Grid.Column width={4}>
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder="Select Designer Brand"
              isSearchable
              value={this.state.designer}
              name="designer"
              isDisabled={this.state.category === ""}
              onChange={this.handleChange.bind(this, "designer")}
              options={designerOptions}
            />
          </Grid.Column>
          <Grid.Column width={4}>
            <Select
              className="basic-single"
              classNamePrefix="select"
              placeholder="Select Item Type"
              isSearchable
              value={this.state.type}
              name="type"
              isDisabled={this.state.designer === ""}
              onChange={this.handleChange.bind(this, "type")}
              options={typeOptions.filter(obj => {
                return obj.category === this.state.category.value;
              })}
            />
          </Grid.Column>
          <Grid.Column width={3}>
            <Button
                fluid
                secondary
                style={{ whiteSpace:'nowrap'}}
              disabled={this.state.type === ""}
              onClick={this.itemAdded}
              type="submit"
            >
              + Add Item
            </Button>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}
