import React from "react";
import { Wizard, Steps, Step, withWizard } from "react-albus";
import {
  Button,
  Checkbox,
  Form,
  Rating,
  Header,
    Icon,
    Grid,
    Segment
} from "semantic-ui-react";
import CreatableSelect from "react-select/lib/Creatable";
import Select from "react-select";
import { ControlLabel, FormControl, FormGroup } from "react-bootstrap";
import "./SteppedForm.css";

let colourOptions = [{ value: "af", text: "Afghanistan" }];
let brandOptions = [
  { value: "saint_laurent_paris", text: "Saint Laurent Paris" },
  { value: "gucci", text: "Gucci" },
  { value: "thom_browne", text: "Thom Browne" },
  { value: "dior", text: "Dior" },
  { value: "fendi", text: "Fendi" },
  { value: "haider_ackermann", text: "Haider Ackermann" }
];
let brandOptions2 = [
  { value: "saint_laurent_paris", label: "Saint Laurent Paris" },
  { value: "gucci", label: "Gucci" },
  { value: "thom_browne", label: "Thom Browne" },
  { value: "dior", label: "Dior" },
  { value: "fendi", label: "Fendi" },
  { value: "haider_ackermann", label: "Haider Ackermann" }
];
let sizeOptions = [{ value: "32", text: "32" }, { value: "31", text: "31" }];
let sizeOptions2 = [{ value: "32", label: "32" }, { value: "31", label: "31" }];
let yearOptions = [
  { value: "vintage", label: "Vintage" },
  { value: "2013", label: "2013" },
  { value: "2014", label: "2014" },
  { value: "2015", label: "2015" },
  { value: "2016", label: "2016" },
  { value: "2017", label: "2017" },
  {
    value: "2018",
    label: "2018"
  }
];

let sizeCategorySizeOptions = [
  {
    category: "clothes",
    options: [
      { value: "extra-small", label: "XS" },
      { value: "small", label: "S" },
      {
        value: "medium",
        label: "M"
      },
      { value: "large", label: "L" },
      { value: "extra-large", label: "XL" }
    ]
  },
  {
    category: "shoes",
    options: [
      { value: "5", label: "5" },
      { value: "6", label: "6" },
      { value: "7", label: "7" },
      {
        value: "8",
        label: "8"
      },
      { value: "9", label: "9" }
    ]
  }
];

let categoryTypeOptions = [
  {
    type: "clothes",
    options: [
      { value: "outerwear", label: "Outerwear" },
      {
        value: "sweats-knits",
        label: "Knitwear/Sweats"
      },
      { value: "shirts-t-shirts", label: "Shirts/T-Shirts" },
      { value: "trousers", label: "Trousers" }
    ]
  },
  {
    type: "shoes",
    options: [
      { value: "boots", label: "Boots" },
      {
        value: "formal-shoes",
        label: "Formal Shoes"
      },
      { value: "casual-shoes", label: "Casual Shoes" }
    ]
  }
];

class myComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.wizard.step.id);
    return (
      <h1
        className={
          "step " + (this.props.wizard.step.id === "1" ? "active" : "completed")
        }
      >
        hi
      </h1>
    );
  }
}

class ProgressSideBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.wizard.step.id);

    return (
        <div>
      <div className="ui vertical fluid steps">
        <div
          className={
            "step " +
            (this.props.wizard.step.id === "1" ? "active" : "") +
            (parseInt(this.props.wizard.step.id) > 1 ? "completed" : "")
          }
        >
          <i className="question circle icon" />
          <div className="content">
            <div className="title">1 - Details</div>
          </div>
        </div>
        <div
          className={
            "step " +
            (this.props.wizard.step.id === "2" ? "active" : "") +
            (parseInt(this.props.wizard.step.id) > 2 ? "completed" : "")
          }
        >
          <i className="info icon" />
          <div className="content">
            <div className="title">2 - Information</div>
          </div>
        </div>
        <div
          className={
            "step " + (this.props.wizard.step.id === "3" ? "active" : "")
          }
        >
          <i className="camera icon" />
          <div className="content">
            <div className="title">3 - Images</div>
          </div>
        </div>
        </div>
            <Grid columns='equal'>
                <Grid.Column>
                    {this.props.wizard.step.id !== "1" ?
                    <Button fluid icon basic labelPosition='left' onClick={this.props.wizard.previous}>
                        <Icon name="left arrow"/>
                        Previous
                    </Button> : ""}
                </Grid.Column>
                <Grid.Column>
                    {this.props.wizard.step.id !== "3" ?
                    <Button fluid icon secondary labelPosition='right' onClick={this.props.wizard.next}>
                        Next
                        <Icon name="right arrow"/>
                    </Button> : ""}
                </Grid.Column>
            </Grid>
            {this.props.wizard.step.id === "3" ?
                <div className="ui divider" /> : ""}
            {this.props.wizard.step.id === "3" ?
                <Button secondary fluid onClick={this.handleSubmit} type="submit">
                    Submit
                </Button> : ""}


        </div>

    );
  }
}

const Wrapped = withWizard(ProgressSideBar);

export default class SteppedForm extends React.Component {
  state = {
    selectedType: "",
    selectedCategory: "",
    selectedDesigner: "",
    selectedSize: "",
    selectedCondition: "",
    selectedPurchaseYear: "",
    selectedReceipt: ""
  };
  handleChange = (e, { value }) => {
    this.setState({ value });
    this.setState({ selectedType: value });
  };

  handleCategoryChange = (inputValue: any, actionMeta: any) => {
    this.setState({ selectedCategory: inputValue });
  };

  handleTypeChange = (e, { value }) => {
    this.setState({ selectedType: value });
    this.setState({ selectedCategory: "" });
    this.setState({ selectedSize: "" });
  };

  handleDesignerChange = (inputValue: any, actionMeta: any) => {
    this.setState({ selectedDesigner: inputValue });
  };

  isValidNewOption = (inputValue, selectValue, selectOptions) => {
    return !(
      inputValue.trim().length === 0 ||
      selectOptions.find(option => option.name === inputValue)
    );
  };

  handleRSChange = (newValue: any, actionMeta: any) => {
    console.group("Value Changed");
    this.setState({ selectedSize: newValue });
    console.log(newValue);
    console.log(`action: ${actionMeta.action}`);
    console.groupEnd();
  };
  handleInputChange = (inputValue: any, actionMeta: any) => {
    console.group("Input Changed");
    console.log(inputValue);
    console.log(`action: ${actionMeta.action}`);
    console.groupEnd();
  };

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
  };

  customOnChange = (option, { action }) => {
    this.setState({ selectedSize: option });
  };

  render() {
    return (
      <Wizard>
          <div className="container">
        <div className="ui stackable grid ">
            <div className="two column row">
                    <div className="column six wide">

                    <Header as='h1' textAlign='center'>Sell us your item!</Header>
                    </div>
                </div>
          <div className="two column row">
            <div className="column six wide">
                    <Wrapped />

            </div>
            <div className="column 10 wide">

              <Form>
                <Steps>
                  <Step
                    id="1"
                    render={({ next }) => (
                      <div>
                        <Form.Field required>
                          <label>Designer</label>
                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            placeholder="Start typing..."
                            isClearable
                            isSearchable
                            value={this.state.selectedDesigner}
                            name="brands"
                            onChange={this.handleDesignerChange}
                            options={brandOptions2}
                          />
                        </Form.Field>
                        <div className="ui divider" />
                        <label>Type</label>
                        <div className="inline fields">
                          <div className="field">
                            <Form.Field>
                              <Checkbox
                                radio
                                label="Clothing"
                                name="checkboxRadioGroup"
                                value="clothes"
                                checked={this.state.selectedType === "clothes"}
                                onChange={this.handleTypeChange}
                              />
                            </Form.Field>
                          </div>
                          <div className="field">
                            <Form.Field>
                              <Checkbox
                                radio
                                label="Shoes"
                                name="checkboxRadioGroup"
                                value="shoes"
                                checked={this.state.selectedType === "shoes"}
                                onChange={this.handleTypeChange}
                              />
                            </Form.Field>
                          </div>
                          <div className="field">
                            <Form.Field>
                              <Checkbox
                                radio
                                label="Accessories"
                                name="checkboxRadioGroup"
                                value="accessories"
                                checked={
                                  this.state.selectedType === "accessories"
                                }
                                onChange={this.handleTypeChange}
                              />
                            </Form.Field>
                          </div>
                        </div>
                        <Form.Field>
                          <div className="inline field required">
                            <label>Category</label>
                            <Select
                              className="basic-single"
                              classNamePrefix="select"
                              placeholder="Start typing..."
                              isClearable
                              value={this.state.selectedCategory}
                              isSearchable
                              isDisabled={this.state.selectedType === ""}
                              onChange={this.handleCategoryChange}
                              name="brands"
                              // options={brandOptions2}
                              options={categoryTypeOptions.filter(obj => {
                                return obj.type === this.state.selectedType;
                              })}
                            />
                          </div>
                        </Form.Field>
                        <div className="ui divider" />
                        <Form.Field required>
                          <label>Size</label>
                          <CreatableSelect
                            isClearable
                            placeholder="Start typing..."
                            getOptionLabel={option => option.label}
                            getOptionValue={option => option.value}
                            isValidNewOption={this.isValidNewOption}
                            getNewOptionData={(inputValue, optionLabel) => ({
                              value: inputValue,
                              label: optionLabel
                            })}
                            value={this.state.selectedSize}
                            onChange={this.customOnChange}
                            onInputChange={this.handleInputChange}
                            isDisabled={this.state.selectedCategory === ""}
                            options={sizeCategorySizeOptions.filter(obj => {
                              return obj.category === this.state.selectedType;
                            })}
                          />
                          <div className="ui pointing label">
                            As indicated on label
                          </div>
                        </Form.Field>
                        <div className="ui divider" />

                      </div>
                    )}
                  />
                  <Step
                    id="2"
                    render={({ next, previous }) => (
                      <div>
                        <Form.Field required>
                          <label>Condition</label>
                          <Rating
                            icon="star"
                            defaultRating={5}
                            maxRating={10}
                          />
                        </Form.Field>
                        <div className="ui divider" />
                        <Form.Field>
                          <label>Year of purchase</label>
                          <Select
                            className="basic-single"
                            classNamePrefix="select"
                            placeholder="Select a year"
                            isClearable
                            name="brands"
                            options={yearOptions}
                          />
                        </Form.Field>
                        <Form.Field
                          control={Checkbox}
                          label={<label>Proof of Purchase</label>}
                        />
                        <Form.Field
                          control={Checkbox}
                          label={<label>Original Tags/Packaging</label>}
                        />
                        <div className="ui divider" />

                      </div>
                    )}
                  />
                  <Step
                    id="3"
                    render={({ previous }) => (
                      <div>
                        <FormGroup controlId="file">
                          <ControlLabel>Front Photo</ControlLabel>
                          <FormControl
                            onChange={this.handleFileChange}
                            type="file"
                          />
                        </FormGroup>
                        <FormGroup controlId="file">
                          <ControlLabel>Back Photo</ControlLabel>
                          <FormControl
                            onChange={this.handleFileChange}
                            type="file"
                          />
                        </FormGroup>
                        <FormGroup controlId="file">
                          <ControlLabel>Label Photo</ControlLabel>
                          <FormControl
                            onChange={this.handleFileChange}
                            type="file"
                          />
                        </FormGroup>

                      </div>
                    )}
                  />
                </Steps>
              </Form>
            </div>
          </div>
        </div>
          </div>
      </Wizard>
    );
  }
}

//size, condition, year of purchase, receipt
//Image upload
