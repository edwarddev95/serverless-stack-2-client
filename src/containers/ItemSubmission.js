import React from "react";
import "./TRR.css";
import ItemSelector from "../components/ItemSelector";
import { Header, Container, Divider, Grid, Button, Icon, Message, Image} from "semantic-ui-react";
import DetailedItemSelector from "../components/DetailedItemSelector";
import SubmissionListDisplay from "../components/SubmissionListDisplay";
import "./ItemSubmission.css";

const SubmitButton = ({ displayed }) => {
    if (!displayed) {
        return "";
    }
    if (displayed) {
        return (
            <Grid stackable>
                <Grid.Row centered>
                    <Grid.Column width={8}>


                        <Button.Group fluid >
                            <Button type="add" onClick={()=>window.scrollTo(0, 0)}>
                                Add Another Item
                            </Button>
                            <Button.Or />
                            <Button secondary type="submit">
                                Submit for evaluation
                            </Button>
                        </Button.Group>



                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
};

export default class FormExampleForm extends React.Component {
    state = {
        selectedItems: []
    };

    onSubmit = (addedItem) => {
        console.log("function called");
        console.log(addedItem);

        this.setState({
            selectedItems: [...this.state.selectedItems, addedItem]
        });
    };

    deleteItem = index => {
        console.log(index);
        let array = [...this.state.selectedItems]; // make a separate copy of the array
        array.splice(index, 1);
        this.setState({ selectedItems: array });
    };

    render() {
        return (
            <Container>
                <div>
                    <Header as='h2' icon textAlign='center'>
                        <Icon name='money bill alternate outline' circular />
                        <Header.Content>Submit your Luxury, High-End and High-Value fashion items in 3 easy steps</Header.Content>
                    </Header>
                </div>
                <Divider fitted></Divider>

                <DetailedItemSelector onClick ={this.onSubmit}/>
                {this.state.selectedItems.length > 0 ? (
                    <SubmissionListDisplay
                        deleteItem={this.deleteItem.bind(this)}
                        items={this.state.selectedItems}
                    />
                ) : (
                    ""
                )}

                <SubmitButton displayed={this.state.selectedItems.length > 0} />

            </Container>

        );
    }
}
