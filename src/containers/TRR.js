import React from "react";
import "./TRR.css";
import ItemSelector from "../components/ItemSelector";
import { Header, Container, Divider, Grid, Button, Icon} from "semantic-ui-react";
import SubmissionListDisplay from "../components/SubmissionListDisplay";

const SubmitButton = ({ displayed }) => {
  if (!displayed) {
    return "";
  }
  if (displayed) {
    return (
      <Grid stackable>
        <Grid.Row centered>
          <Grid.Column width={8}>


              <Button.Group fluid >
                  <Button type="add" onClick={()=>window.scrollTo(0, 0)}>
                      Add Another Item
                  </Button>
                  <Button.Or />
                  <Button secondary type="submit">
                      Submit for evaluation
                  </Button>
              </Button.Group>



          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
};

export default class FormExampleForm extends React.Component {
  state = {
    selectedItems: []
  };

  handleSubmit = selectedItem => {
    this.setState({
      selectedItems: [...this.state.selectedItems, selectedItem]
    });
    console.log(this.state.selectedItems);
  };

  deleteItem = index => {
    console.log(index);
    let array = [...this.state.selectedItems]; // make a separate copy of the array
    array.splice(index, 1);
    this.setState({ selectedItems: array });
  };

  render() {
    return (
      <Container>
        <Header as="h1" dividing textAlign="center">
          Sell us your items!
        </Header>
        <ItemSelector handleSubmit={this.handleSubmit.bind(this)} />
        {this.state.selectedItems.length > 0 ? (
          <SubmissionListDisplay
            deleteItem={this.deleteItem.bind(this)}
            items={this.state.selectedItems}
          />
        ) : (
          ""
        )}

        <SubmitButton displayed={this.state.selectedItems.length > 0} />
      </Container>
    );
  }
}
