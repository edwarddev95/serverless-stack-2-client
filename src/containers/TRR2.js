import React from "react";
import "./TRR.css";
import ItemSelector from "../components/ItemSelector";
import { Header, Container, Divider, Grid, Button, Icon} from "semantic-ui-react";
import SubmissionCardList from "../components/SubmissionCardList";

let item1 = {
    designer: "Gucci",
    type: "Men's Fashion & Accessories",
    category: "Sweatshirts & Knitwear"
};

let item2 = {
    designer: "Saint Laurent",
    type: "Men's Fashion & Accessories",
    category: "Sweatshirts & Knitwear"
};

let item3 = {
    designer: "Fendi",
    type: "Men's Fashion & Accessories",
    category: "Sweatshirts & Knitwear"
};

let items =[item1, item2, item3]

export default class AddItemDetails extends React.Component {

    render() {
        return (
            <Container>
                <Divider horizontal>Step 2 of 3</Divider>
                <Header as="h1" dividing textAlign="center">
                    Add some details!
                </Header>
                <SubmissionCardList items={items}/>
            </Container>
        );
    }
}
