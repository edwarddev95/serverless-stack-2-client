import React from "react";
import { Rating, Button, Checkbox, Form } from "semantic-ui-react";
import CreatableSelect from "react-select/lib/Creatable";
import Select from "react-select";
import { ControlLabel, FormControl, FormGroup } from "react-bootstrap";

// import { countryOptions } from '../common'
const colourOptions = [{ value: "af", text: "Afghanistan" }];
const brandOptions = [
  { value: "saint_laurent_paris", text: "Saint Laurent Paris" },
  { value: "gucci", text: "Gucci" },
  { value: "thom_browne", text: "Thom Browne" },
  { value: "dior", text: "Dior" },
  { value: "fendi", text: "Fendi" },
  { value: "haider_ackermann", text: "Haider Ackermann" }
];
const brandOptions2 = [
  { value: "saint_laurent_paris", label: "Saint Laurent Paris" },
  { value: "gucci", label: "Gucci" },
  { value: "thom_browne", label: "Thom Browne" },
  { value: "dior", label: "Dior" },
  { value: "fendi", label: "Fendi" },
  { value: "haider_ackermann", label: "Haider Ackermann" }
];
const sizeOptions = [{ value: "32", text: "32" }, { value: "31", text: "31" }];
const sizeOptions2 = [{ value: "32", label: "32" }, { value: "31", label: "31" }];
const yearOptions = [
  { value: "vintage", label: "Vintage" },
  { value: "2013", label: "2013" },
  { value: "2014", label: "2014" },
  { value: "2015", label: "2015" },
  { value: "2016", label: "2016" },
  { value: "2017", label: "2017" },
  {
    value: "2018",
    label: "2018"
  }
];

const sizeCategorySizeOptions = [
  {
    category: "clothes",
    options: [
      { value: "extra-small", label: "XS" },
      { value: "small", label: "S" },
      {
        value: "medium",
        label: "M"
      },
      { value: "large", label: "L" },
      { value: "extra-large", label: "XL" }
    ]
  },
  {
    category: "shoes",
    options: [
      { value: "5", label: "5" },
      { value: "6", label: "6" },
      { value: "7", label: "7" },
      {
        value: "8",
        label: "8"
      },
      { value: "9", label: "9" }
    ]
  }
];

const categoryTypeOptions = [
  {
    type: "clothes",
    options: [
      { value: "outerwear", label: "Outerwear" },
      {
        value: "sweats-knits",
        label: "Knitwear/Sweats"
      },
      { value: "shirts-t-shirts", label: "Shirts/T-Shirts" },
      { value: "trousers", label: "Trousers" }
    ]
  },
  {
    type: "shoes",
    options: [
      { value: "boots", label: "Boots" },
      {
        value: "formal-shoes",
        label: "Formal Shoes"
      },
      { value: "casual-shoes", label: "Casual Shoes" }
    ]
  }
];

{
  /* Selected value: <b>{this.state.value}</b> */
}

export default class FormExampleForm extends React.Component {
  state = {
    selectedType: "",
    selectedCategory: "",
    selectedDesigner: "",
    selectedSize: "",
    selectedCondition: "",
    selectedPurchaseYear: "",
    selectedReceipt: ""
  };

  handleChange = (e, { value }) => {
    this.setState({ value });
    this.setState({ selectedType: value });
  };

  handleCategoryChange = (inputValue: any, actionMeta: any) => {
    this.setState({ selectedCategory: inputValue });
  };

  handleTypeChange = (e, { value }) => {
    this.setState({ selectedType: value });
    this.setState({ selectedCategory: "" });
    this.setState({ selectedSize: "" });
  };

  handleDesignerChange = (inputValue: any, actionMeta: any) => {
    this.setState({ selectedDesigner: inputValue });
  };

  isValidNewOption = (inputValue, selectValue, selectOptions) => !(
      inputValue.trim().length === 0 ||
      selectOptions.find(option => option.name === inputValue)
    );

  handleRSChange = (newValue: any, actionMeta: any) => {
    console.group("Value Changed");
    this.setState({ selectedSize: newValue });
    console.log(newValue);
    console.log(`action: ${actionMeta.action}`);
    console.groupEnd();
  };

  handleInputChange = (inputValue: any, actionMeta: any) => {
    console.group("Input Changed");
    console.log(inputValue);
    console.log(`action: ${actionMeta.action}`);
    console.groupEnd();
  };

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
  };

  customOnChange = (option, { action }) => {
    this.setState({ selectedSize: option });
  };

  render() {
    return (
      <div className="ui one column stackable center aligned page grid">
        <div className="column twelve wide">
          <div className="ui form">
            <Form>
              <Form.Field>
                <label>Designer</label>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  placeholder="Start typing..."
                  isClearable
                  isSearchable
                  value={this.state.selectedDesigner}
                  name="brands"
                  onChange={this.handleDesignerChange}
                  options={brandOptions2}
                />
              </Form.Field>
              <div className="ui divider" />
              <label>Type</label>
              <div className="inline fields">
                <div className="field">
                  <Form.Field>
                    <Checkbox
                      radio
                      label="Clothing"
                      name="checkboxRadioGroup"
                      value="clothes"
                      checked={this.state.selectedType === "clothes"}
                      onChange={this.handleTypeChange}
                    />
                  </Form.Field>
                </div>
                <div className="field">
                  <Form.Field>
                    <Checkbox
                      radio
                      label="Shoes"
                      name="checkboxRadioGroup"
                      value="shoes"
                      checked={this.state.selectedType === "shoes"}
                      onChange={this.handleTypeChange}
                    />
                  </Form.Field>
                </div>
                <div className="field">
                  <Form.Field>
                    <Checkbox
                      radio
                      label="Accessories"
                      name="checkboxRadioGroup"
                      value="accessories"
                      checked={this.state.selectedType === "accessories"}
                      onChange={this.handleTypeChange}
                    />
                  </Form.Field>
                </div>
              </div>
              <Form.Field>
                <div className="inline field">
                  <label>Category</label>
                  <Select
                    className="basic-single"
                    classNamePrefix="select"
                    placeholder="Start typing..."
                    isClearable
                    value={this.state.selectedCategory}
                    isSearchable
                    isDisabled={this.state.selectedType === ""}
                    onChange={this.handleCategoryChange}
                    name="brands"
                    // options={brandOptions2}
                    options={categoryTypeOptions.filter(obj => obj.type === this.state.selectedType)}
                  />
                </div>
              </Form.Field>
              <div className="ui divider" />
              <Form.Field>
                <label>Size</label>
                <CreatableSelect
                  isClearable
                  placeholder="Start typing..."
                  getOptionLabel={option => option.label}
                  getOptionValue={option => option.value}
                  isValidNewOption={this.isValidNewOption}
                  getNewOptionData={(inputValue, optionLabel) => ({
                    value: inputValue,
                    label: optionLabel
                  })}
                  value={this.state.selectedSize}
                  onChange={this.customOnChange}
                  onInputChange={this.handleInputChange}
                  isDisabled={this.state.selectedCategory === ""}
                  options={sizeCategorySizeOptions.filter(obj => obj.category === this.state.selectedType)}
                />
                <div className="ui pointing label">As indicated on label</div>
              </Form.Field>
              <div className="ui divider" />
              <Form.Field>
                <label>Condition</label>
                <Rating icon="star" defaultRating={5} maxRating={10} />
              </Form.Field>
              <Form.Field>
                <label>Year of purchase</label>
                <Select
                  className="basic-single"
                  classNamePrefix="select"
                  placeholder="Select a year"
                  isClearable
                  name="brands"
                  options={yearOptions}
                />
              </Form.Field>
              <div className="ui divider" />
              <Form.Field
                control={Checkbox}
                label={<label>Proof of Purchase</label>}
              />
              <Form.Field
                control={Checkbox}
                label={<label>Original Tags/Packaging</label>}
              />
              <div className="ui divider" />
              <FormGroup controlId="file">
                <ControlLabel>Front Photo</ControlLabel>
                <FormControl onChange={this.handleFileChange} type="file" />
              </FormGroup>
              <FormGroup controlId="file">
                <ControlLabel>Back Photo</ControlLabel>
                <FormControl onChange={this.handleFileChange} type="file" />
              </FormGroup>
              <FormGroup controlId="file">
                <ControlLabel>Label Photo</ControlLabel>
                <FormControl onChange={this.handleFileChange} type="file" />
              </FormGroup>
              <Button onClick={this.handleSubmit} type="submit">
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
