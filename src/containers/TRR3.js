import React from "react";
import "./TRR.css";
import ItemSelector from "../components/ItemSelector";
import { Header, Container, Divider, Grid, Button, Icon} from "semantic-ui-react";
import DetailedItemSelector from "../components/DetailedItemSelector";
import SubmissionListDisplay from "../components/SubmissionListDisplay";
const SubmitButton = ({ displayed }) => {
    if (!displayed) {
        return "";
    }
    if (displayed) {
        return (
            <Grid stackable>
                <Grid.Row centered>
                    <Grid.Column width={8}>


                        <Button.Group fluid >
                            <Button type="add" onClick={()=>window.scrollTo(0, 0)}>
                                Add Another Item
                            </Button>
                            <Button.Or />
                            <Button secondary type="submit">
                                Submit for evaluation
                            </Button>
                        </Button.Group>



                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
};

export default class FormExampleForm extends React.Component {
    state = {
        selectedItems: []
    };

    render() {
        return (
            <Container>
                <Header as="h1" dividing textAlign="center">
                    Sell us your items!
                </Header>
                <DetailedItemSelector/>
                {this.state.selectedItems.length > 0 ? (
                    <SubmissionListDisplay
                        deleteItem={this.deleteItem.bind(this)}
                        items={this.state.selectedItems}
                    />
                ) : (
                    ""
                )}

                <SubmitButton displayed={this.state.selectedItems.length > 0} />
            </Container>
        );
    }
}
